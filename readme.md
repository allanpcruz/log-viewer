# Requisições Diárias - Relatório HTML

Este é um pequeno projeto em Node.js que analisa arquivos de log contendo informações sobre requisições diárias em um sistema de e-commerce. O script lê os arquivos de log, extrai dados relevantes e gera um relatório em formato HTML, exibindo as informações agrupadas por data, método HTTP, URL e quantidade de requisições.

## Funcionalidades

- **Leitura de Arquivos de Log**: O script lê arquivos de log no diretório especificado em *logsDirectory*.
- **Análise de Requisições**: Analisa as requisições contidas nos arquivos de log, identificando método HTTP, URL e quantidade de requisições.
- **Geração de Relatório HTML**: Gera um arquivo HTML chamado output.html com as informações agrupadas por data, método HTTP, URL e quantidade de requisições.

## Estrutura do Código

O código está estruturado da seguinte forma:

- **Leitura de Arquivos:**
    - Utiliza o módulo fs para ler os arquivos de log no diretório especificado.
- **Processamento das Requisições:**
    - Analisa cada linha do arquivo de log, identificando método HTTP e URL das requisições.
    - Agrupa as requisições por data, método HTTP e URL, contabilizando a quantidade de requisições para cada grupo.
- **Geração do Relatório HTML:**
    - Gera um arquivo HTML estruturado com as informações agrupadas.
    - Utiliza estilos CSS para formatar a tabela e tornar o relatório visualmente atraente.

## Como Usar

1. **Pré-requisitos:**
    - Certifique-se de ter Node.js instalado em seu sistema.

2. **Execução:**
    - Clone o repositório em seu ambiente local.
    - Abra o terminal e navegue até o diretório do projeto.
    - Execute o script usando o comando ***node nome-do-arquivo.js***.
    - Após a execução bem-sucedida, o arquivo output.html será gerado no mesmo diretório.

3. **Visualização do Relatório:**
    - Abra o arquivo output.html em um navegador web para visualizar o relatório de requisições diárias.

## Notas Adicionais

- O script assume que os arquivos de log estão no diretório especificado e seguem o padrão de nomenclatura request-AAAA-MM-DD.log.
- Certifique-se de que o diretório de saída tenha permissões de escrita para que o arquivo HTML possa ser gerado com sucesso.
- 
Esperamos que este pequeno projeto seja útil para analisar e visualizar dados de requisições diárias em sua api. 

Em caso de dúvidas ou sugestões de melhorias, sinta-se à vontade para entrar em contato.

**Obrigado por usar este script!**