const fs = require('fs');
const path = require('path');

console.log(process.argv);

if (process.argv.length < 3) {
    console.error('Uso: node script.js <caminho-do-diretorio-de-logs>');
    process.exit(1);
}

const logsDirectory = process.argv[2];
const logFiles = fs.readdirSync(logsDirectory);
const requestsByDate = {};

logFiles.forEach((file) => {
    if (file.startsWith('request-') && file.endsWith('.log')) {
        const regex = /^request-(\d{4})-(\d{2})-(\d{2})\.log$/;
        const match = file.match(regex);
        const dataArquivo = `${match[3]}/${match[2]}/${match[1]}`;

        const filePath = path.join(logsDirectory, file);
        const logData = fs.readFileSync(filePath, 'utf8').trim().split('\n');

        let method, url;
        logData.forEach((logLine) => {
            if (logLine.includes('method')) {
                method = logLine.match(/"method": "(.*?)"/)[1];
            }
            if (logLine.includes('url')) {
                url = logLine.match(/"url": "(.*?)"/)[1].replace(/\/\d+$/, '').replace(/\/$/, '');

            }
            if (method && url) {
                if (!requestsByDate[dataArquivo]) {
                    requestsByDate[dataArquivo] = [];
                }

                const existingRequestIndex = requestsByDate[dataArquivo].findIndex(request => request.metodo === method && request.url === url);
                if (existingRequestIndex !== -1) {
                    requestsByDate[dataArquivo][existingRequestIndex].quantidade++;
                } else {
                    requestsByDate[dataArquivo].push({
                        data: dataArquivo,
                        metodo: method,
                        url: url,
                        quantidade: 1
                    });
                }
                method = null;
                url = null;
            }
        });
    }
});

const htmlContent = `
<!DOCTYPE html>
<html>
    <head>
        <title>Requisições Diárias</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
                margin-top: 100px;
            }
            h1 {
                background-color: #333;
                color: #fff;
                padding: 20px;
                text-align: center;
                margin: 0;
                width: 100%;
                position: fixed;
                top: 0;
                z-index: 1000;
            }
            table {
                width: 80%;
                margin: 20px auto;
                border-collapse: collapse;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }
            th {
                background-color: #f4f4f4;
                text-align: center;
            }
            tr:nth-child(even) {
                background-color: #f9f9f9;
            }
            tr:hover {
                background-color: #f5f5f5;
            }
            pre {
                white-space: pre-wrap;
            }
            .quantidade {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <h1>Requisições Diárias</h1>
            ${Object.keys(requestsByDate).map(date => `
                <table>
                    <thead>
                        <tr>
                            <th id="data-header">Data do Arquivo</th>
                            <th id="method-header">Método</th>
                            <th id="url-header">URL</th>
                            <th id="quantidade-header">Quantidade</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${requestsByDate[date].map(request => `
                            <tr>
                                <td>${request.data}</td>
                                <td>${request.metodo}</td>
                                <td>${request.url}</td>
                                <td class="quantidade">${request.quantidade}</td>
                            </tr>
                        `).join('')}
                    </tbody>
                </table>
            `).join('')}
    </body>
</html>
`;

fs.writeFileSync('output.html', htmlContent, 'utf8');

console.log('HTML gerado com sucesso em output.html');
